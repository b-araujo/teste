# Teste


#TESTE
> - Deve ser criado uma tela com que filtre `ids` de produtos, o máximo de filtros que conseguir.
> - nao se preocupe com o layout.
a meta é conseguir identificar o resultado (id's,pId's)
> - na chave `sort` do json contem todos os dados necessários para criação do filtro e na chave `products` o resultado
>_____________________________________
>OBS.:
>deve ser criado uma tela com alguns `combobox` com os filtros.
> deve ser usado um usuário de teste no login no `header`
>`basic access authentication`
>User: `peter_p`
>Pass: `123456`

##URL:
>`www.wishare.com.br/code/products/getProductsFilters/orderby:[brand ou price]/order:[desc ou asc].json`

##Parametros
> _Todos os parâmetros devem ser passados mesmo que vazios_
>`data[filters][featured] = true ou false `

>`data[filters][size]  = ["P","M"]` #É uma string, um json para o servidor interpretar

>`data[filters][color] `

>`data[filters][brand] `

>`data[filters][store] `

>`data[filter][text] `

##Paginação
>`data[page] = 1 (obrigatorio, sempre iniciado em 1 e ir incrementado a cada requisição)`]
